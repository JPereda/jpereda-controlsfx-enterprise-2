package org.controlsfx.control.tableview2;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.tableview2.actions.ColumnFixAction;
import org.controlsfx.control.tableview2.actions.RowFixAction;
import org.controlsfx.control.tableview2.cell.ComboBox2TableCell;
import org.controlsfx.control.tableview2.cell.TextField2TableCell;
import org.controlsfx.control.tableview2.filter.filtereditor.SouthFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupNumberFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupStringFilter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.List;

public class Test3TVvsFTV extends Application {
    
    private final ObservableList<Person> data = Person.generateData(100);
    private final ObservableList<Person> dataOrig = Person.generateData(100);
    
    private final FilteredTableView<Person> table = new FilteredTableView<>();
    private final FilteredTableColumn<Person, String> firstName = new FilteredTableColumn<>("First Name");
    private final FilteredTableColumn<Person, String> lastName = new FilteredTableColumn<>("Last Name");
    private final FilteredTableColumn<Person, Integer> age = new FilteredTableColumn<>("Age");
    private final FilteredTableColumn<Person, Color> color = new FilteredTableColumn<>("Color");
    private final FilteredTableColumn<Person, String> city = new FilteredTableColumn<>("City");
    private final FilteredTableColumn<Person, LocalDate> birthday = new FilteredTableColumn<>("Birthday");
    private final FilteredTableColumn<Person, Boolean> active = new FilteredTableColumn<>("Active");
    
    private final TableView<Person> tableOrig = new TableView<>();
    private final TableColumn<Person, String> firstNameOrig = new TableColumn<>("First Name");
    private final TableColumn<Person, String> lastNameOrig = new TableColumn<>("Last Name");
    private final TableColumn<Person, Integer> ageOrig = new TableColumn<>("Age");
    private final TableColumn<Person, String> cityOrig = new TableColumn<>("City");
    private final TableColumn<Person, LocalDate> birthdayOrig = new TableColumn<>("Birthday");
    private final TableColumn<Person, Boolean> activeOrig = new TableColumn<>("Active");

    SouthFilter<Person, String> editorFirstName;
    SouthFilter<Person, String> editorLastName;
    SouthFilter<Person, Integer> editorAge;
    SouthFilter<Person, Color> editorColor;

    private final VBox controls = new VBox(10);
    
    @Override
    public void start(Stage primaryStage) {
        setTableView();
        setTableView2();
        setControls();
        final HBox hBox = new HBox(50, tableOrig, controls, table);
        HBox.setHgrow(tableOrig, Priority.ALWAYS);
        HBox.setHgrow(table, Priority.ALWAYS);
        hBox.setPadding(new Insets(20));
        Scene scene = new Scene(hBox, 1200, 550);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
       
        table.getFixedRows().addAll(0, 1, 2);
//        table.getFixedColumns().add(fullNameColumn);
//        table.getFixedColumns().addAll(firstName, lastName);
    }
    
    private void setTableView2() {
        firstName.setCellValueFactory(p -> p.getValue().firstNameProperty());
//        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setCellFactory(ComboBox2TableCell.forTableColumn("Name 1", "Name 2", "Name 3", "Name 4"));
        firstName.setPrefWidth(110);
        
        lastName.setCellValueFactory(p -> p.getValue().lastNameProperty());
//        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setCellFactory(TextField2TableCell.forTableColumn());
        lastName.setPrefWidth(130);


        age.setCellValueFactory(p -> p.getValue().ageProperty().asObject());
        age.setCellFactory(TextField2TableCell.forTableColumn(new StringConverter<Integer>() {
            @Override
            public String toString(Integer object) {
                return String.valueOf(object);
            }

            @Override
            public Integer fromString(String string) {
                return Integer.parseInt(string);
            }
        }));
        age.setPrefWidth(90);

        color.setCellValueFactory(p -> p.getValue().colorProperty());
        color.setCellFactory(p -> new TableCell<Person, Color>() {
            private final HBox box;
            private final Circle circle;
            {
                circle = new Circle(10);
                box = new HBox(circle);
                box.setAlignment(Pos.CENTER);
                setText(null);
            }
            @Override
            protected void updateItem(Color item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && ! empty) {
                    circle.setFill(item);
                    setGraphic(box);
                } else {
                    setGraphic(null);
                }
            }
            
        });
        color.setPrefWidth(90);
        
        city.setCellValueFactory(p -> p.getValue().cityProperty());
//        city.setCellFactory(TextFieldTableCell.forTableColumn());
        city.setCellFactory(p -> new TableCell<Person, String>() {
            
            private final Button button;
            {
                button = new Button();
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty); 
                if (item != null && ! empty) {
                    button.setText(item);
                    if (getIndex() % 5 == 0) {
                        setStyle("-fx-font-size: 2em;");
                    } else {
                        setStyle("-fx-font-size: 1em;");
                    }
                    setText(null);
                    setGraphic(button);
                } else {
                    setText(null);
                    setGraphic(null);
                    setStyle(null);
                }
            }
            
        });

        birthday.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthday.setPrefWidth(100);
        birthday.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        active.setText("Active");
        active.setCellValueFactory(p -> p.getValue().activeProperty());
        active.setCellFactory(CheckBoxTableCell.forTableColumn(active));
        active.setPrefWidth(60);
        
        table.setItems(data);
        TableColumn fullNameColumn = new TableColumn("Full Name");
        fullNameColumn.getColumns().addAll(firstName, lastName);
        table.getColumns().addAll(fullNameColumn, age, color, city, birthday, active);
//        table.getColumns().addAll(firstName, lastName, city, birthday, active);
        setUpFilter();
        
        ContextMenu cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(fullNameColumn)));
        fullNameColumn.setContextMenu(cm);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(firstName)));
        final CheckMenuItem miFirstName = new CheckMenuItem("Filter " + firstName.getText());
        firstName.predicateProperty().addListener(o -> miFirstName.setSelected(firstName.getPredicate() != null));
        miFirstName.setOnAction(e ->
            firstName.setPredicate(miFirstName.isSelected() ? p -> p.contains("1") : null));
        final Menu menuFirstName = new Menu("Filter");
        menuFirstName.getItems().addAll(miFirstName);
        cm.getItems().addAll(menuFirstName);
        firstName.setContextMenu(cm);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(lastName)));
        final CheckMenuItem miLastName = new CheckMenuItem("Filter " + lastName.getText());
        lastName.predicateProperty().addListener(o -> miLastName.setSelected(lastName.getPredicate() != null));
        miLastName.setOnAction(e -> 
            lastName.setPredicate(miLastName.isSelected() ? p -> p.contains("2") : null));
        
        final Menu menuLastName = new Menu("Filter");
        menuLastName.getItems().addAll(miLastName);
        cm.getItems().addAll(menuLastName);
        lastName.setContextMenu(cm);

        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(age)));
        final CheckMenuItem miAge = new CheckMenuItem("Filter " + age.getText());
        age.predicateProperty().addListener(o -> miAge.setSelected(age.getPredicate() != null));
        miAge.setOnAction(e ->
                age.setPredicate(miAge.isSelected() ? p -> p > 50 : null));

        final Menu menuAge = new Menu("Filter");
        menuAge.getItems().addAll(miAge);
        cm.getItems().addAll(menuAge);
        age.setContextMenu(cm);

        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(city)));
        city.setContextMenu(cm);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(birthday), ActionUtils.ACTION_SEPARATOR));
        final CheckMenuItem miBirthday = new CheckMenuItem("Filter " + birthday.getText());
        birthday.predicateProperty().addListener(o -> miBirthday.setSelected(birthday.getPredicate() != null));
        miBirthday.setOnAction(e -> 
            birthday.setPredicate(miBirthday.isSelected() ? p -> p.isAfter(LocalDate.of(1950, 1, 1)) : null));
        
        final Menu menuBirthday = new Menu("Filter");
        menuBirthday.getItems().addAll(miBirthday);
        cm.getItems().addAll(menuBirthday);
        birthday.setContextMenu(cm);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFixAction(active)));
        CheckMenuItem miActive = new CheckMenuItem("Filter " + active.getText());
        
        miActive.setOnAction(e -> active.setPredicate(miActive.isSelected() ? p -> p : null));
        final Menu menuActive = new Menu("Filter");
        menuActive.getItems().addAll(miActive);
        cm.getItems().addAll(menuActive);
        active.setContextMenu(cm);


        table.setRowHeaderContextMenuFactory((i, person) -> {
            ContextMenu rowCM = ActionUtils.createContextMenu(Arrays.asList(new RowFixAction(table, i), ActionUtils.ACTION_SEPARATOR));
            if (person != null) {
                final MenuItem menuItem = new MenuItem("Remove  " + person.getFirstName());
                menuItem.setOnAction(e -> {
                    if (i >= 0) {
                        final ObservableList<Person> items = table.getItems();
                        if (items instanceof SortedList) {
                            int sourceIndex = ((SortedList<Person>) items).getSourceIndexFor(data, i);
                            data.remove(sourceIndex);
                        } else {
                            data.remove(i.intValue());
                        }
                    }
                });
                rowCM.getItems().add(menuItem);
            }
            final MenuItem menuItemAdd = new MenuItem("Add new Person");
            menuItemAdd.setOnAction(e -> {
                data.add(new Person());
            });
            rowCM.getItems().add(menuItemAdd);
            return rowCM; 
        });
        
        table.getFixedColumns().add(city);
    }

    private void setTableView() {
        firstNameOrig.setCellValueFactory(p -> p.getValue().firstNameProperty());
        //firstNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameOrig.setCellFactory(p -> {
            ComboBoxTableCell<Person, String> cell = new ComboBoxTableCell<>("Name 1", "Name 2", "Name 3", "Name 4");
            cell.setComboBoxEditable(true);
            return cell;
        });
        firstNameOrig.setPrefWidth(110);
        
        lastNameOrig.setCellValueFactory(p -> p.getValue().lastNameProperty());
        lastNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameOrig.setPrefWidth(130);

        ageOrig.setCellValueFactory(p -> p.getValue().ageProperty().asObject());
        ageOrig.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>() {
            @Override
            public String toString(Integer object) {
                return String.valueOf(object);
            }

            @Override
            public Integer fromString(String string) {
                return Integer.parseInt(string);
            }
        }));
        ageOrig.setPrefWidth(130);

        cityOrig.setCellValueFactory(p -> p.getValue().cityProperty());
        cityOrig.setCellFactory(TextFieldTableCell.forTableColumn());
//        cityOrig.setStyle("-fx-font-size: 2em;");
        
        birthdayOrig.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthdayOrig.setPrefWidth(100);
        birthdayOrig.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        activeOrig.setText("Active");
        activeOrig.setCellValueFactory(p -> p.getValue().activeProperty());
        activeOrig.setCellFactory(CheckBoxTableCell.forTableColumn(activeOrig));
        activeOrig.setPrefWidth(60);
        
        tableOrig.setItems(dataOrig);
        TableColumn column = new TableColumn("Full Name");
        column.getColumns().addAll(firstNameOrig, lastNameOrig);
        tableOrig.getColumns().addAll(column, cityOrig, birthdayOrig, activeOrig);
        
    }
    
    private void setControls() {
    
        CheckBox tableEditionEnabled = new CheckBox("Table Edition Enabled");
        tableEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.setEditable(nv);
            tableOrig.setEditable(nv);
        });
        tableEditionEnabled.setSelected(true);
        
        CheckBox columnsEditionEnabled = new CheckBox("Columns Edition Enabled");
        columnsEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
            tableOrig.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
        });
        columnsEditionEnabled.setSelected(true);
        
        CheckBox cellSelectionEnabled = new CheckBox("Cell Selection Enabled");
        cellSelectionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setCellSelectionEnabled(nv);
            tableOrig.getSelectionModel().setCellSelectionEnabled(nv);
        });
        
        CheckBox selectionMultiple = new CheckBox("Selection Multiple");
        selectionMultiple.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
            tableOrig.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
        });
        
        CheckBox columnPolicy = new CheckBox("Column Policy Constrained");
        columnPolicy.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
            tableOrig.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
        });
        
        CheckBox fixedCellSize = new CheckBox("Set Fixed Cell Size");
        fixedCellSize.selectedProperty().addListener((obs, ov, nv) -> {
            table.setFixedCellSize(nv ? 40 : 0);
            tableOrig.setFixedCellSize(nv ? 40 : 0);
        });
        
        CheckBox showTableMenuButton = new CheckBox("Show Table Menu Button");
        showTableMenuButton.selectedProperty().addListener((obs, ov, nv) -> {
            table.setTableMenuButtonVisible(nv);
            tableOrig.setTableMenuButtonVisible(nv);
        });
        
        CheckBox showData = new CheckBox("Show Data");
        CheckBox sortedList = new CheckBox("Use SortedList");
        showData.setSelected(true);
        showData.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                if (sortedList.isSelected()) {
                    FilteredTableView.configureForFiltering(table, data);
                } else {
                    table.setItems(data);
                }
            } else {
                table.setItems(null);
            }

            tableOrig.setItems(nv ? dataOrig : null);
        });

        sortedList.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                FilteredTableView.configureForFiltering(table, data);
            } else {
                table.setItems(data);
            }
            
            if (nv) {
                FilteredList<Person> filteredData = new FilteredList<>(dataOrig, p -> true);
                SortedList<Person> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(tableOrig.comparatorProperty());
                tableOrig.setItems(sortedData);
            } else {
                tableOrig.setItems(dataOrig);
            }
        });
        
        controls.getChildren().addAll(new Label("Common API"), new Separator(), 
                tableEditionEnabled, columnsEditionEnabled, 
                cellSelectionEnabled, selectionMultiple, columnPolicy, fixedCellSize, showTableMenuButton, showData, sortedList);
        
        CheckBox showRowHeader = new CheckBox("Show Row Header");
        showRowHeader.selectedProperty().addListener((obs, ov, nv) -> {
            table.setRowHeaderVisible(nv);
        });
        
        CheckBox columnFixing = new CheckBox("Column Fixing Enabled");
        columnFixing.setSelected(true);
        columnFixing.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnFixingEnabled(nv);
        });
        
        CheckBox rowFixing = new CheckBox("Row Fixing Enabled");
        rowFixing.setSelected(true);
        rowFixing.selectedProperty().addListener((obs, ov, nv) -> {
            table.setRowFixingEnabled(nv);
        });
        
        CheckBox southFilter = new CheckBox("Use SouthFilter");
        southFilter.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                southNodeFilterAction();
                firstName.setSouthNode(editorFirstName);
                lastName.setSouthNode(editorLastName);
                age.setSouthNode(editorAge);
                color.setSouthNode(editorColor);
            } else {
                filterAction();
                firstName.setSouthNode(null);
                lastName.setSouthNode(null);
                age.setSouthNode(null);
                color.setSouthNode(null);
            }
        });
        
        CheckBox blendSouthFilter = new CheckBox("Blend SouthNode");
        blendSouthFilter.disableProperty().bind(southFilter.selectedProperty().not());
        blendSouthFilter.setSelected(true);
        blendSouthFilter.selectedProperty().addListener((obs, ov, nv) -> {
            table.setSouthHeaderBlended(nv);
        });
        
        CheckBox rowFactory = new CheckBox("Use Row Header Factory");
        rowFactory.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                FilteredTableColumn<Person, Number> tc = new FilteredTableColumn<>();
                final MenuItem menuItem = new MenuItem("Reset Filter");
                menuItem.disableProperty().bind(table.predicateProperty().isNull());
                menuItem.setOnAction(e -> table.resetFilter());
                tc.setContextMenu(new ContextMenu(menuItem));
                tc.setOnFilterAction(e -> {
                    if (table.getPredicate() != null) {
                        table.resetFilter();
                    }
                });
                
                tc.setCellValueFactory(p -> p.getValue().getTotalSum());
                tc.setCellFactory(p -> new TableCell<Person, Number>() {
                    private final HBox box;
                    private final Circle circle;
                    private final Label label;
                    {
                        circle = new Circle(5); 
                        label = new Label(); 
                        box = new HBox(10, circle, label); 
                    }
                    
                    @Override
                    protected void updateItem(Number item, boolean empty) {
                        super.updateItem(item, empty); 
                        if (item != null && ! empty) {
                            setText(null);
                            circle.setFill(getIndex() % 5 == 0 ? Color.RED : Color.BLUE);
                            label.setText("" + table.getItems().get(getIndex()).getBirthday().getYear() + " " + String.valueOf(item));
                            box.setAlignment(Pos.CENTER);
                            setGraphic(box);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }

                });
                table.setRowHeader(tc);
            } else {
                table.setRowHeader(null);
            }

            table.setRowHeaderWidth(nv ? 100 : 40);
        });
        
        controls.getChildren().addAll(new Label("TableView2 API"), new Separator(), showRowHeader, columnFixing, rowFixing, 
                southFilter, blendSouthFilter, rowFactory);
        controls.setAlignment(Pos.CENTER_LEFT);
        controls.setMinWidth(200);
    }

    private void setUpFilter() {

        editorFirstName = new SouthFilter<>(firstName, String.class);

        editorLastName = new SouthFilter<>(lastName, String.class);

        editorAge = new SouthFilter<>(age, Integer.class);

        editorColor = new SouthFilter<>(color, Color.class);
        editorColor.getFilterEditor().setCellFactory(c -> new ListCell<Color>() {
            private final HBox box;
            private final Circle circle;
            {
                circle = new Circle(10);
                box = new HBox(circle);
                box.setAlignment(Pos.CENTER);
                setText(null);
            }
            @Override
            protected void updateItem(Color item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && ! empty) {
                    circle.setFill(item);
                    setGraphic(box);
                } else {
                    setGraphic(null);
                }
            }
            
        });
        editorColor.getFilterEditor().setConverter(new StringConverter<Color>() {
            @Override
            public String toString(Color object) {
                return object != null && colors.indexOf(object) > -1 ? 
                        scolors.get(colors.indexOf(object)) : editorColor.getFilterEditor().getEditor().getText();
            }

            @Override
            public Color fromString(String string) {
                if (string == null || string.isEmpty()) 
                    return Color.CADETBLUE;
                try {
                    if (Color.web(string) != null) {
                        return Color.web(string);
                    }
                } catch (Exception e) {}
                return Color.CADETBLUE;
            }
        });

        filterAction();
    }

    private final List<Color> colors = Arrays.asList(Color.CADETBLUE, Color.CHARTREUSE, Color.CHOCOLATE, Color.CORAL, Color.CORNSILK, Color.CORNFLOWERBLUE);
    private final List<String> scolors = Arrays.asList("cadetblue", "chartreuse", "chocolate", "coral", "cornsilk", "cornflowerblue");
        
    private void southNodeFilterAction() {
        firstName.setOnFilterAction(e -> {
            if (firstName.getPredicate() != null) {
                editorFirstName.getFilterEditor().cancelFilter();
            }
        });
        lastName.setOnFilterAction(e -> {
            if (lastName.getPredicate() != null) {
                editorLastName.getFilterEditor().cancelFilter();
            }
        });
        age.setOnFilterAction(e -> {
            if (age.getPredicate() != null) {
                editorAge.getFilterEditor().cancelFilter();
            }
        });
        
        color.setOnFilterAction(e -> {
            if (color.getPredicate() != null) {
                editorColor.getFilterEditor().cancelFilter();
            }
        });
    }

    private void filterAction() {
        PopupFilter<Person, String> popupFirstNameFilter = new PopupStringFilter<>(firstName);
        firstName.setOnFilterAction(e -> popupFirstNameFilter.showPopup());

        PopupFilter<Person, String> popupLastNameFilter = new PopupStringFilter<>(lastName);
        lastName.setOnFilterAction(e -> popupLastNameFilter.showPopup());

        PopupFilter<Person, Integer> popupAgeFilter = new PopupNumberFilter<>(age);
        age.setOnFilterAction(e -> popupAgeFilter.showPopup());
        
        PopupStringFilter<Person, Color> popupColorFilter = new PopupStringFilter<>(color);
        popupColorFilter.setConverter(new StringConverter<Color>() {
            @Override
            public String toString(Color object) {
                return object != null ? scolors.get(colors.indexOf(object)) : "";
            }

            @Override
            public Color fromString(String string) {
                return string == null || string.isEmpty() ? Color.CADETBLUE : Color.web(string);
            }
        });
        color.setOnFilterAction(e -> popupColorFilter.showPopup());
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
