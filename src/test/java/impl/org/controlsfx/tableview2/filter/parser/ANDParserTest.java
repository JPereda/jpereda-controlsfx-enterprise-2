package impl.org.controlsfx.tableview2.filter.parser;

import impl.org.controlsfx.tableview2.filter.parser.number.NumberParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ANDParserTest {

    private NumberParser<Integer> parser;

    @Before
    public void setUp() {
        parser = new NumberParser<>();
    }

    @Test
    public void isGreaterThanAndLessThan1() {
        Assert.assertTrue(parser.parse("> 100 AND < 150").test(120));
    }

    @Test
    public void isGreaterThanAndLessThan2() {
        Assert.assertFalse(parser.parse("> 100 AND < 150").test(160));
    }

    @Test
    public void ANDInsideOR() {
        Assert.assertTrue(parser.parse("= 90 OR ≠ 160 AND > 100").test(90));
    }
}
